import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Plateau{

    private int nbLignes;
    private int nbColonnes;
    private int pourcentageDeBombes;
    private int nbBombes;
    private List<List<CaseIntelligente>> lePlateau;

    /**
     * Initialise un plateau
     * 
     * @param nbLignes (int) nombre de lignes du plateau
     * @param nbColonnes (int) nombre de colonnes du plateau
     * @param pourcentageDeBombes (int) pourcentage de bombes
     */
    public Plateau(int nbLignes, int nbColonnes, int pourcentageDeBombes){
        this.nbLignes = nbLignes;
        this.nbColonnes = nbColonnes;
        this.nbBombes = 0;
        this.pourcentageDeBombes = pourcentageDeBombes;
        this.lePlateau = new ArrayList<List<CaseIntelligente>>();
        this.creerLesCasesVides();
        this.rendLesCasesIntelligentes();
        this.poseDesBombesAleatoirement();
    }

    /**
     * Créer les cases vides du plateau sous forme de listes de listes
     */
    private void creerLesCasesVides(){
        for (int ligne=0; ligne < this.nbLignes; ligne++){
            this.lePlateau.add(new ArrayList<CaseIntelligente>());
            for (int colonne=0; colonne < this.nbColonnes; colonne++){
                this.lePlateau.get(ligne).add(new CaseIntelligente());
            }
        }
    }

    /**
     * Rend les cases intelligentes
     */
    private void rendLesCasesIntelligentes(){
        for (int ligne=0; ligne < this.nbLignes; ligne++){
            for (int colonne=0; colonne < this.nbColonnes; colonne++){
                CaseIntelligente caseActuelle = this.getCase(ligne, colonne);
                if (this.estSurLePlateau(ligne-1, colonne-1)){
                    CaseIntelligente caseVoisine = this.getCase(ligne-1, colonne-1);
                    caseActuelle.ajouteVoisine(caseVoisine);
                }
                if (this.estSurLePlateau(ligne, colonne-1)){
                    CaseIntelligente caseVoisine1 = this.getCase(ligne, colonne-1);
                    caseActuelle.ajouteVoisine(caseVoisine1);
                }
                if (this.estSurLePlateau(ligne+1, colonne-1)){
                    CaseIntelligente caseVoisine2 = this.getCase(ligne+1, colonne-1);
                    caseActuelle.ajouteVoisine(caseVoisine2);
                }
                if (this.estSurLePlateau(ligne-1, colonne)){
                    CaseIntelligente caseVoisine3 = this.getCase(ligne-1, colonne);
                    caseActuelle.ajouteVoisine(caseVoisine3);
                }
                if (this.estSurLePlateau(ligne+1, colonne)){
                    CaseIntelligente caseVoisine4 = this.getCase(ligne+1, colonne);
                    caseActuelle.ajouteVoisine(caseVoisine4);
                }
                if (this.estSurLePlateau(ligne-1, colonne+1)){
                    CaseIntelligente caseVoisine5 = this.getCase(ligne-1, colonne+1);
                    caseActuelle.ajouteVoisine(caseVoisine5);
                }
                if (this.estSurLePlateau(ligne, colonne+1)){
                    CaseIntelligente caseVoisine6 = this.getCase(ligne, colonne+1);
                    caseActuelle.ajouteVoisine(caseVoisine6);
                }
                if (this.estSurLePlateau(ligne+1, colonne+1)){
                    CaseIntelligente caseVoisine7 = this.getCase(ligne+1, colonne+1);
                    caseActuelle.ajouteVoisine(caseVoisine7);
                }
            }
        }
    }

    /**
     * pose les bombes sur le plateau de façon aléatoire
     */
    protected void poseDesBombesAleatoirement(){
        Random generateur = new Random();
        for (int x = 0; x < this.getNbLignes(); x++){
            for (int y = 0; y < this.getNbColonnes(); y++){
                if (generateur.nextInt(100)+1 < this.pourcentageDeBombes){
                    this.poseBombe(x, y);
                    this.nbBombes = this.nbBombes + 1;
                }
            }
        }
    }

    /**
     * Savoir si la case de coordonnées x et y est sur le plateau
     * 
     * @param numLigne (int) coordonnées x de la case
     * @param numColonne (int) coordonnées y de la case
     * 
     * @return (boolean) : true si la case de coordonnées x et y est sur le plateau, sinon false
     */
    public boolean estSurLePlateau(int numLigne, int numColonne){
        return numLigne >= 0 && numLigne < this.nbLignes && numColonne >= 0 && numColonne < this.nbColonnes;
    }

    /**
     * Retourne le nombre de lignes du plateau
     * @return nombre de lignes
     */
    public int getNbLignes(){
        return this.nbLignes;
    }

    /**
     * Retourne le nombre de colonnes du plateau
     * @return nombre de colonnes
     */
    public int getNbColonnes(){
        return this.nbColonnes;
    }

    /**
     * Retourne le pourcentage de bombes du plateau
     * 
     * @return le pourcentage de bombes
     */
    public int getPourcentageDeBombes(){
        return this.pourcentageDeBombes;
    }

    /**
     * Retourne le nombre total de bombes du plateau
     * 
     * @return nombre de bombes
     */
    public int getNbTotalBombes(){
        return this.nbBombes;
    }

    /**
     * Récupérer la case sur le plateau de coordonnées x et y
     * @param numLigne (int) coordonnées x de la case
     * @param numColonne (int) coordonnées y de la case
     * 
     * @return (CaseIntelligente) : la case intelligente de coordonées x et y
     */
    public CaseIntelligente getCase(int numLigne, int numColonne){
        return this.lePlateau.get(numLigne).get(numColonne);
    }

    /**
     * Retourne le nombre de cases marquées sur le plateau
     * 
     * @return (int) : le nombre de cases marquées
     */
    public int getNbCasesMarquees(){
        int nbCasesMarquees = 0;
        for (int ligne=0; ligne < this.nbLignes; ligne++){
            for (int colonne=0; colonne < this.nbColonnes; colonne++){
                CaseIntelligente caseActuelle = this.getCase(ligne, colonne);
                if (caseActuelle.estMarquee()){
                    nbCasesMarquees += 1;
                }
            }
        }
        return nbCasesMarquees;
    }

    /**
     * Pose une bombe sur une case précise
     * 
     * @param x (int) coordonnées x de la case
     * @param y (int) coordonnées y de la case
     */
    public void poseBombe(int x, int y){
        this.getCase(x, y).poseBombe();
    }

    /**
     * Reinitialise le plateau
     */
    public void reset(){
        for (List<CaseIntelligente> ligne : this.lePlateau){
            for (CaseIntelligente caseIntelligente : ligne){
                caseIntelligente.reset();
            }
        }
        this.nbBombes = 0;
    }
}