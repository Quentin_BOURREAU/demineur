import java.util.Scanner;

public class Demineur extends Plateau{
    private boolean gameOver;
    private int score;
    private boolean premiereCase;

    /**
     * Instancie le jeu Demineur
     * 
     * @param nbLignes (int) : le nombre de lignes du plateau de jeu Demineur
     * @param nbColonnes (int) : le nombre de colonnes du plateau de jeu Demineur
     * @param pourcentage (int) : le pourcentage de bombes sur le plateau de jeu Demineur
     */
    public Demineur(int nbLignes, int nbColonnes, int pourcentage){
        super(nbLignes, nbColonnes, pourcentage);
        this.gameOver = false;
        this.score = 0;
        this.premiereCase = true;
    }

    /**
     * Retourne le score de la partie
     * 
     * @return (int) : le score de la partie
     */
    public int getScore(){
        return this.score;
    }

    /**
     * Révèle la case choisie de coordonées x et y sur le plateau de jeu
     * 
     * @param x (int) : la coordonnée en abscisses de la case choisie
     * @param y (int) : la coordonnée en ordonnées de la case choisie
     */
    public void reveler(int x, int y){
        if (this.premiereCase){
            CaseIntelligente caseActuelle = super.getCase(x, y);
            for (Case caseVoisine : caseActuelle.getVoisines()){
                if (!caseVoisine.contientBombe()){
                    caseVoisine.reveler();
                    this.score += 1;
                }
            }
            caseActuelle.reveler();
            this.score += 1;
            this.premiereCase = false;
        }
        else{
            CaseIntelligente caseActuelle = super.getCase(x, y);
            if (!caseActuelle.estMarquee()){
                if (caseActuelle.contientBombe()){
                    this.gameOver = true;
                }
                else{
                    caseActuelle.reveler();
                    this.score += 1;
                }
            }
        }
    }

    /**
     * Marque la case de coordonnées x et y sur le plateau de jeu
     * 
     * @param x (int) : la coordonnée en abscisses de la case choisie
     * @param y (int) : la coordonnée en ordonnées de la case choisie
     * @return (boolean) : true si la case a bien été marquée, sinon false
     */
    public boolean marquer(int x, int y){
        CaseIntelligente caseActuelle = super.getCase(x,y);
        if (!caseActuelle.estDecouverte()){
            caseActuelle.marquer();
            return true;
        }
        return false;
    }

    /**
     * Retourne true si la partie est gagnée, sinon false
     * 
     * @return (boolean) : true si la partie est gagnée, sinon false
     */
    public boolean estGagnee(){
        return this.score == this.getNbColonnes()*this.getNbLignes() - this.getNbTotalBombes();
    }

    /**
     * Retourne true si la partie est perdue, sinon false
     * 
     * @return (boolean) : true si la partie est perdue, sinon false
     */
    public boolean estPerdue(){
        return this.gameOver;
    }

    /**
     * Remet à true pour la prochaine partie que lors de la première case découverte, on découvre aussi les cases voisines n'étant pas des bombes
     */
    public void setPremiereCase(){
        this.premiereCase = true;
    }

    /**
     * Réinitialise la partie
     */
    @Override
    public void reset(){
        this.score = 0;
        this.gameOver = false;
        super.reset();
    }

    /**
     * Affiche la partie en cours
     */
    public void affiche(){
        System.out.println("JEU DU DEMINEUR");
        // affichage de la bordure supérieure
        System.out.print("  ");
        for (int j=0; j<this.getNbColonnes(); j++){
            System.out.print("  "+j+" ");
        }
        System.out.print(" \n");
        
        // affichage des numéros de ligne + cases
        System.out.print("  ┌");
        for (int j=0; j<this.getNbColonnes()-1; j++){
                System.out.print("───┬");
        }
        System.out.println("───┐");
        
        // affichage des numéros de ligne + cases
        for (int i = 0; i<this.getNbLignes(); i++){
            System.out.print(i+" ");
            for (int j=0; j<this.getNbColonnes(); j++){
                System.out.print("│ "+this.getCase(i, j).toString() + " ");
            }
            System.out.print("│\n");
            
            if (i!=this.getNbLignes()-1){
                // ligne milieu
                System.out.print("  ├");
                for (int j=0; j<this.getNbColonnes()-1; j++){
                        System.out.print("───┼");
                }
                System.out.println("───┤");
            }
        }

        // affichage de la bordure inférieure
        System.out.print("  └");
        for (int j=0; j<this.getNbColonnes()-1; j++){
                System.out.print("───┴");
        }
        System.out.println("───┘");
        
        // affichage des infos 
        System.out.println("Nombres de bombes à trouver : " + this.getNbTotalBombes());
        System.out.println("Nombres de cases marquées : " + this.getNbCasesMarquees());
        System.out.println("Score : " + this.getScore());
    }

    /**
     * Lance une nouvelle partie
     */
    public void nouvellePartie(){
        this.reset();
        this.poseDesBombesAleatoirement();
        this.affiche();
        Scanner scan = new Scanner(System.in).useDelimiter("\n");

        while (!this.estPerdue() || this.estGagnee()){
            System.out.println("Entrer une instruction de la forme R 3 2 ou M 3 2\npour Révéler/Marquer la case à la ligne 3 et à la colonne 2");
            String [] s = scan.nextLine().split(" ");
            String action = s[0];
            int x = Integer.valueOf(s[1]);
            int y = Integer.valueOf(s[2]);
            if (action.equals("M") || action.equals("m"))
                this.marquer(x, y);
            else if (action.equals("R") || action.equals("r"))
                this.reveler(x, y);
            this.affiche();
        }
        if (this.gameOver){
            System.out.println("Oh !!! Vous avez perdu !");
        }
        else{
            System.out.println("Bravo !! Vous avez gagné !");
        }
    }
}