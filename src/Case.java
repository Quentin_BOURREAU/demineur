public class Case{

    private boolean contientUneBombe;
    private boolean estDecouverte;
    private boolean estMarquee;

    /**
     * Instancie un case 
     */
    public Case(){
        this.contientUneBombe = false;
        this.estDecouverte = false;
        this.estMarquee = false;
    }

    /**
     * Réinitialise une case en mettant ses attributs à false
     */
    public void reset(){
        this.contientUneBombe = false;
        this.estDecouverte = false;
        this.estMarquee = false;
    }

    /**
     * Passe l'attribut contientUneBombe à true
    */
    public void poseBombe(){
        this.contientUneBombe = true;
    }

    /**
     * Passe l'attribut estMarquee à true
     */
    public void marquer(){
        this.estMarquee = !estMarquee;
    }

    /**
     * Renvoie si la case est découverte ou non
     * @return (boolean) : si la case est découverte ou non
     */
    public boolean estDecouverte(){
        return this.estDecouverte;
    }   

    /**
     * Renvoie si la case est marquée ou non
     * @return (boolean) : si la case est marquée ou non
     */
    public boolean estMarquee(){
        return this.estMarquee;
    }


    /**
     * Renvoie si la case est contient une bombe ou non
     * @return (boolean) : si la case contient une bombe ou non
     */
    public boolean contientBombe(){
        return this.contientUneBombe;
    }

    /**
     * Tente de révéler la case est renvoie un booléen en focntion du résultat (true si la case est bien révélée sinon false)
     * @return (boolean) : 
     */
    public boolean reveler(){
        if (this.contientUneBombe || this.estMarquee){
            return false;
        }
        this.estDecouverte = true;
        return true;
    }
}