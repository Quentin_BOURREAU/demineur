import java.util.ArrayList;
import java.util.List;

public class CaseIntelligente extends Case{
    private List<Case> lesVoisines;

    /**
     * Instancie une case intelligente en héritant de la classe Case
     */
    public CaseIntelligente(){
        super();
        this.lesVoisines = new ArrayList<>();
    }

        /**
     * Renvoie la liste des cases voisines à la case instanciée
     * 
     * @return List<Case> : la liste des cases voisines à la case instanciée
     */
    public List<Case> getVoisines(){
        return this.lesVoisines;
    }

    /**
     * Ajoute à la liste de cases voisines (lesVoisines) une nouvelle case voisine
     * 
     * @param uneCase (Case) : une case voisine de la case intelligente instanciée
     */
    public void ajouteVoisine(Case uneCase){
        this.lesVoisines.add(uneCase);
    }

    /**
     * Retourne le nombre de bombes voisines de la case intelligente instanciée
     * 
     * @return (int) : le nombre de bombes voisines de la case intelligente instanciée
     */
    public int nombreBombesVoisines(){
        int nbBombes = 0;
        for (Case caseActuelle : this.lesVoisines){
            if (caseActuelle.contientBombe()){
                nbBombes += 1;
            }
        }
        return nbBombes;
    }

    /**
     * Retourne un affichage pour la case intelligente instanciée
     * 
     * @return (String) : l'affichage de la case intelligente instanciée
     */
    @Override
    public String toString(){
        if (super.estMarquee()){ // la case est marquée
            return "🚩";
        }
        else if (super.estDecouverte()){
            if (super.contientBombe()){ // la case est révélée et elle contient une bombe
                return "@";
            }
            else{ // la case est révélée et elle ne contient pas de bombe
                return "" + this.nombreBombesVoisines();
            }
        }
        return " "; // la case n'est ni révélée ni marquée

    }
}