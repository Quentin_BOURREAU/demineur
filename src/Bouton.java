import javafx.scene.control.Button; 
import javafx.scene.image.ImageView;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill ;
import javafx.scene.paint.Color ;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class Bouton extends Button{
    
    private CaseIntelligente laCase;

    public Bouton(CaseIntelligente laCase){
        super();
        this.setPrefWidth(35);
        this.setPrefHeight(35);
        this.laCase = laCase;        
    }

    public void maj(){
        if (this.laCase.estDecouverte()){  
            this.setText(this.laCase.toString());
            this.setFont(Font.font("Arial", FontWeight.BOLD, 15)); // Change le style du texte en gras et de taille 15
            int nbBombesAutour = this.laCase.nombreBombesVoisines();
            if (nbBombesAutour == 0){
                this.setTextFill(Color.BLACK); // Change la couleur du texte en noir
            }
            if (nbBombesAutour == 1){
                this.setTextFill(Color.BLUE); // Change la couleur du texte en bleu
            }
            if (nbBombesAutour == 2){
                this.setTextFill(Color.GREEN); // Change la couleur du texte en vert
            }
            if (nbBombesAutour == 3){
                this.setTextFill(Color.RED); // Change la couleur du texte en rouge
            }
            if (nbBombesAutour >= 4){
                this.setTextFill(Color.PURPLE); // Change la couleur du texte en violet
            }
            this.setDisable(true);          
            if (this.laCase.contientBombe()){
                this.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
            }
            else{
                this.setBackground(new Background(new BackgroundFill(Color.GREENYELLOW, null, null)));
            }
        }
        else if (this.laCase.estMarquee()){
            this.setText(this.laCase.toString());
            this.setFont(Font.font("Arial", FontWeight.BOLD, 15)); // Change le style du texte en gras et de taille 15
            this.setTextFill(Color.BLACK); // Change la couleur du texte en noir
            this.setBackground(new Background(new BackgroundFill(Color.GAINSBORO, null, null)));           
            }
        else{
            this.setDisable(false);
            this.setText(""); // Remet le texte à vide
            this.setBackground(new Background(new BackgroundFill(Color.GAINSBORO, null, null)));
        }
    }
}
