# Développement en Java d'une application du jeu Démineur

## Principe du jeu
Le démineur est un jeu dont le but est de découvrir toutes les cases libres sans faire exploser les mines, chaque case étant soit une bombe, soit libre. Au début du jeu toutes les cases sont cachées et en cliquant sur une case on la révèle, c’est-à-dire qu’on découvre son contenu : si c’est une bombe on perd, sinon, le nombre de cases adjacentes qui sont des bombes s’affiche sur la case. On peut également marquer des cases par un drapeau ou un ? en cliquant sur le bouton droit de la souris.

## Fonctionnalités
Bien évidemment, nous avons implémenté le jeu Démineur demandé. Néanmoins, nous avons rajouté quelques fonctionnalités. Premièrement, les cases marquées ne sont plus de couleur jaune et avec un "?" dessus, elle sont maintenant de la même couleur que le jeu et avec un drapeau dedans pour rendre plus esthétique et agréable à jouer. Ensuite, nous avons modifié les couleurs des cases découvertes suivant le nombre de bombes à distance (cases voisines) et aggrandi la police des numéros pour améliorer encore l'esthétique de notre application. Effectivement, si ce nombre est de 0, la couleur est le noir ; pour 1, celui-ci est de couleur bleu ; pour le 2, la couleur du numéro est rouge et enfin lorsque le numéro de bombes voisines d'une case est supérieur ou égal à 4, la couleur du numéro est donc violet. Puis, nous avons ajouté également la fonctionnalité de lors de la première case révélée, celle-ci révèle toutes les cases voisines n'étant pas des bombes pour faciliter le début d'une partie.


## Lien de notre gitlab
https://gitlab.com/Quentin_BOURREAU/demineur

#

BOURREAU Quentin
JACQUET Noa
BUT info 1.3.A